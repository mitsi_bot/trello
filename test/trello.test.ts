import axios, { AxiosResponse } from "axios";
import Trello from "../src/Trello";
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const defaultResponse = [
  {
    "id": "5e92cc805d50cf34b5c29bc3",
    "name": "Todo 1"
  },
  {
    "id": "5e92cc671006de4065ed3939",
    "name": "Todo 2"
  },
  {
    "id": "5e885c65ae0b4158913888f1",
    "name": "Todo 3"
  }
];

const emptyResponse: {
  id: string;
  name: string;
}[] = [];

function axios200Response(data: object): AxiosResponse<any> {
  return {
    data: data,
    status: 200,
    statusText: "OK",
    config: {},
    headers: {}
  }
};

const axios500Response: AxiosResponse<any> = {
  data: null,
  status: 503,
  statusText: "An internal server occured",
  config: {},
  headers: {}
};

describe("Trello", () => {
  it("should repond with data", async () => {
    mockedAxios.get.mockImplementationOnce((url: string) => {
      expect(url).toBe("https://api.trello.com/1/lists/myListId/cards?key=apiKey&token=myToken&fields=name")
      return Promise.resolve(axios200Response(defaultResponse));
    });
    const response = await new Trello("apiKey", "myToken").get("myListId");
    expect(response).toBe(`> Todo 1\n> Todo 2\n> Todo 3`);
  });

  it("should repond nothing todo", async () => {
    mockedAxios.get.mockImplementationOnce((url: string) => {
      expect(url).toBe("https://api.trello.com/1/lists/myListId/cards?key=apiKey&token=myToken&fields=name")
      return Promise.resolve(axios200Response(emptyResponse));
    });
    const response = await new Trello("apiKey", "myToken").get("myListId");
    expect(response).toBe(`Nothing found :)`);
  });

  it("Handle API error", async () => {
    mockedAxios.get.mockImplementationOnce(() => Promise.reject(axios500Response));
    const response = await new Trello("apiKey", "myToken").get("myListId");
    expect(response).toBe(`An error has occurred while checking Trello :'(`);
  });
});
