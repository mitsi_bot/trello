import express, { Response, Request } from "express";
const config = require('config-yml');
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import BaseService from "mitsi_bot_lib/dist/src/BaseService";
import Trello from "./Trello";

class Server extends BaseService {
    onPost(request: Request, response: Response): void {
        const trello = new Trello(config.app.key, config.app.token);
        trello.deprecatedTrello(request.body.parameters)
            .then((formattedList: string) => {
                response.json(new ResponseText(formattedList));
            }).catch((err: Error) => {
                console.error(err);
                response.json(new ResponseText("trello error"));
            });
    }
}

new Server(express()).startServer();