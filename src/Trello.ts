import axios from "axios";
const config = require('config-yml');

export default class Trello {
  private apiKey: string;
  private token: string;

  constructor(apiKey: string, token: string) {
    this.apiKey = apiKey;
    this.token = token;
  }
  
  public deprecatedGetEncours() {
    return this.get(config.app.inProgressId as string)
  }

  public deprecatedGetAFaire() {
    return this.get(config.app.todoId as string)
  }

  public deprecatedTrello(parameters: any): Promise<string> {
    return this.deprecatedGetTrello(parameters != undefined && parameters.fields != undefined && parameters.fields.temporality.stringValue === "");
  }

  private async deprecatedGetTrello(todo: boolean) {
    return !todo ? this.deprecatedGetEncours() : this.deprecatedGetAFaire();
  }

  public async get(listId: string) {
    const url = `https://api.trello.com/1/lists/${listId}/cards?key=${this.apiKey}&token=${this.token}&fields=name`;
    try {
      const response = await axios.get(url);
      const data = response.data || [];
      const result = data.map((c: any) => `> ${c.name}`);
      return result.length > 0 ? result.join("\n") : "Nothing found :)";
    } catch {
      return "An error has occurred while checking Trello :'(";
    }
  }
}